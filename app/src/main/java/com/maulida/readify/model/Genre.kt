package com.maulida.readify.model

class Genre (
    var idGenre: String = "",
    var genre: String = ""
        )

val genre1 = Genre(
    idGenre = "1",
    genre = "Romantis"
)

val genre2 = Genre(
    idGenre = "2",
    genre = "Fantasi"
)

val genre3 = Genre(
    idGenre = "3",
    genre = "Horor"
)

val genre4 = Genre(
    idGenre = "4",
    genre = "Thriller"
)

val genre5 = Genre(
    idGenre = "5",
    genre = "Komedi"
)

val genre6 = Genre(
    idGenre = "6",
    genre = "Aksi"
)

val genre7 = Genre(
    idGenre = "7",
    genre = "Drama"
)

val genre8 = Genre(
    idGenre = "8",
    genre = "Sejarah"
)

val listDataGenre = listOf(genre1, genre2, genre3, genre4, genre5, genre6, genre7, genre8)

